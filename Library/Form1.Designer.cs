﻿
namespace Library
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelMenu = new System.Windows.Forms.Panel();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonAcc = new System.Windows.Forms.Button();
            this.buttonFavor = new System.Windows.Forms.Button();
            this.buttonLibrary = new System.Windows.Forms.Button();
            this.panelLogo = new System.Windows.Forms.Panel();
            this.panelTitle = new System.Windows.Forms.Panel();
            this.pictureBoxClose = new System.Windows.Forms.PictureBox();
            this.labelTitle = new System.Windows.Forms.Label();
            this.panelDesktop = new System.Windows.Forms.Panel();
            this.panelMenu.SuspendLayout();
            this.panelTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxClose)).BeginInit();
            this.SuspendLayout();
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.panelMenu.Controls.Add(this.buttonEdit);
            this.panelMenu.Controls.Add(this.buttonAdd);
            this.panelMenu.Controls.Add(this.buttonAcc);
            this.panelMenu.Controls.Add(this.buttonFavor);
            this.panelMenu.Controls.Add(this.buttonLibrary);
            this.panelMenu.Controls.Add(this.panelLogo);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(220, 629);
            this.panelMenu.TabIndex = 0;
            // 
            // buttonEdit
            // 
            this.buttonEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonEdit.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonEdit.FlatAppearance.BorderSize = 0;
            this.buttonEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEdit.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonEdit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonEdit.Image = global::Library.Properties.Resources.imgonline_com_ua_Negativ_lB5KgIJnN7jeQ_1_;
            this.buttonEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonEdit.Location = new System.Drawing.Point(0, 420);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.buttonEdit.Size = new System.Drawing.Size(220, 80);
            this.buttonEdit.TabIndex = 5;
            this.buttonEdit.Text = "  Редагувати";
            this.buttonEdit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonEdit.UseVisualStyleBackColor = true;
            this.buttonEdit.Visible = false;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonAdd.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAdd.FlatAppearance.BorderSize = 0;
            this.buttonAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdd.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonAdd.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdd.Image = global::Library.Properties.Resources.imgonline_com_ua_Negativ_8ozMP10RyQA_1_;
            this.buttonAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAdd.Location = new System.Drawing.Point(0, 340);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.buttonAdd.Size = new System.Drawing.Size(220, 80);
            this.buttonAdd.TabIndex = 4;
            this.buttonAdd.Text = "  Додати";
            this.buttonAdd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Visible = false;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonAcc
            // 
            this.buttonAcc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonAcc.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAcc.FlatAppearance.BorderSize = 0;
            this.buttonAcc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAcc.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonAcc.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAcc.Image = global::Library.Properties.Resources.imgonline_com_ua_Negativ_TqfuBqKp7eodN0K;
            this.buttonAcc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAcc.Location = new System.Drawing.Point(0, 260);
            this.buttonAcc.Name = "buttonAcc";
            this.buttonAcc.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.buttonAcc.Size = new System.Drawing.Size(220, 80);
            this.buttonAcc.TabIndex = 3;
            this.buttonAcc.Text = "  Аккаунт";
            this.buttonAcc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAcc.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonAcc.UseVisualStyleBackColor = true;
            this.buttonAcc.Click += new System.EventHandler(this.buttonAcc_Click);
            // 
            // buttonFavor
            // 
            this.buttonFavor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonFavor.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonFavor.FlatAppearance.BorderSize = 0;
            this.buttonFavor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFavor.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonFavor.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonFavor.Image = global::Library.Properties.Resources.imgonline_com_ua_Negativ_ocqJEM4msNOWJ;
            this.buttonFavor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonFavor.Location = new System.Drawing.Point(0, 180);
            this.buttonFavor.Name = "buttonFavor";
            this.buttonFavor.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.buttonFavor.Size = new System.Drawing.Size(220, 80);
            this.buttonFavor.TabIndex = 2;
            this.buttonFavor.Text = "  Я читаю";
            this.buttonFavor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonFavor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonFavor.UseVisualStyleBackColor = true;
            this.buttonFavor.Click += new System.EventHandler(this.buttonFavor_Click);
            // 
            // buttonLibrary
            // 
            this.buttonLibrary.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonLibrary.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonLibrary.FlatAppearance.BorderSize = 0;
            this.buttonLibrary.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLibrary.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonLibrary.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonLibrary.Image = global::Library.Properties.Resources.imgonline_com_ua_Negativ_ybScjMODx0FpAxdN1;
            this.buttonLibrary.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonLibrary.Location = new System.Drawing.Point(0, 100);
            this.buttonLibrary.Name = "buttonLibrary";
            this.buttonLibrary.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.buttonLibrary.Size = new System.Drawing.Size(220, 80);
            this.buttonLibrary.TabIndex = 1;
            this.buttonLibrary.Text = "  Бібліотека";
            this.buttonLibrary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonLibrary.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonLibrary.UseVisualStyleBackColor = true;
            this.buttonLibrary.Click += new System.EventHandler(this.buttonLibrary_Click);
            // 
            // panelLogo
            // 
            this.panelLogo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.panelLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLogo.Location = new System.Drawing.Point(0, 0);
            this.panelLogo.Name = "panelLogo";
            this.panelLogo.Size = new System.Drawing.Size(220, 100);
            this.panelLogo.TabIndex = 0;
            // 
            // panelTitle
            // 
            this.panelTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(126)))), ((int)(((byte)(229)))));
            this.panelTitle.Controls.Add(this.pictureBoxClose);
            this.panelTitle.Controls.Add(this.labelTitle);
            this.panelTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitle.Location = new System.Drawing.Point(220, 0);
            this.panelTitle.Name = "panelTitle";
            this.panelTitle.Size = new System.Drawing.Size(962, 100);
            this.panelTitle.TabIndex = 1;
            // 
            // pictureBoxClose
            // 
            this.pictureBoxClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxClose.Image = global::Library.Properties.Resources.imgonline_com_ua_Negativ_7GA25dXlhGW;
            this.pictureBoxClose.Location = new System.Drawing.Point(917, 63);
            this.pictureBoxClose.Name = "pictureBoxClose";
            this.pictureBoxClose.Size = new System.Drawing.Size(33, 31);
            this.pictureBoxClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxClose.TabIndex = 32;
            this.pictureBoxClose.TabStop = false;
            this.pictureBoxClose.Visible = false;
            this.pictureBoxClose.Click += new System.EventHandler(this.pictureBoxClose_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labelTitle.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelTitle.Location = new System.Drawing.Point(430, 34);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(98, 37);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "HOME";
            this.labelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelDesktop
            // 
            this.panelDesktop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDesktop.Location = new System.Drawing.Point(220, 100);
            this.panelDesktop.Name = "panelDesktop";
            this.panelDesktop.Size = new System.Drawing.Size(962, 529);
            this.panelDesktop.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1182, 629);
            this.Controls.Add(this.panelDesktop);
            this.Controls.Add(this.panelTitle);
            this.Controls.Add(this.panelMenu);
            this.Name = "Form1";
            this.Text = "Library";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panelMenu.ResumeLayout(false);
            this.panelTitle.ResumeLayout(false);
            this.panelTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxClose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Button buttonAcc;
        private System.Windows.Forms.Button buttonFavor;
        private System.Windows.Forms.Button buttonLibrary;
        private System.Windows.Forms.Panel panelLogo;
        private System.Windows.Forms.Panel panelTitle;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Panel panelDesktop;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.PictureBox pictureBoxClose;
        private System.Windows.Forms.Button buttonEdit;
    }
}

