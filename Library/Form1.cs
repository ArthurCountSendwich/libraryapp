﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;

namespace Library
{

    public partial class Form1 : Form
    {
        private Form activeForm;

        static public User GlobalUser;
        static public Book GlobalBook;

        public Form1()
        {
            InitializeComponent();
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void OpenChildForm(Form childForm, object buttonSender)
        {
            if (activeForm != null)
            {
                activeForm.Close();
            }
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            this.panelDesktop.Controls.Add(childForm);
            this.panelDesktop.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }
        private void CloseChildForm()
        {
            if (activeForm != null)
            {
                activeForm.Close();
            }
            labelTitle.Text = "Home";
            panelTitle.BackColor = Color.FromArgb(108, 126, 229);
            panelLogo.BackColor = Color.FromArgb(30, 30, 30);
            buttonLibrary.BackColor = Color.FromArgb(38, 38, 38);
            buttonFavor.BackColor = Color.FromArgb(38, 38, 38);
            buttonAcc.BackColor = Color.FromArgb(38, 38, 38);
            buttonAdd.BackColor = Color.FromArgb(38, 38, 38);
            buttonEdit.BackColor = Color.FromArgb(38, 38, 38);
            pictureBoxClose.Visible = false;
        }
        public void Change()
        {
            if (GlobalUser != null)
            {
                if (GlobalUser.IsAdmin == true)
                {
                    buttonEdit.Visible = true;
                    buttonAdd.Visible = true;
                }
                else
                {
                    buttonEdit.Visible = false;
                    buttonAdd.Visible = false;
                }

                Book book = new Book();
                GlobalBook = book.GetFromId(GlobalUser.BookId);


                if (GlobalUser.BookId == 1)
                    WindowsFormsApp1.FormFavor.BookLInk = "none";
                else
                    WindowsFormsApp1.FormFavor.BookLInk = GlobalBook.BookLink;
            }
            else
            {
                buttonEdit.Visible = false;
                buttonAdd.Visible = false;

                WindowsFormsApp1.FormFavor.BookLInk = null;
            }
        }

        private void buttonLibrary_Click(object sender, EventArgs e)
        {
            labelTitle.Text = "Бібліотека";
            buttonLibrary.BackColor = Color.FromArgb(119, 170, 215);
            panelTitle.BackColor = Color.FromArgb(119, 170, 215);
            panelLogo.BackColor = Color.FromArgb(100, 150, 215);
            buttonFavor.BackColor = Color.FromArgb(38, 38, 38);
            buttonAcc.BackColor = Color.FromArgb(38, 38, 38);
            buttonAdd.BackColor = Color.FromArgb(38, 38, 38);
            buttonEdit.BackColor = Color.FromArgb(38, 38, 38);
            OpenChildForm(new Forms.FormLibrary(), sender);
            pictureBoxClose.Visible = true;
            Change();
        }

        private void buttonFavor_Click(object sender, EventArgs e)
        {
            labelTitle.Text = "Я читаю";
            buttonFavor.BackColor = Color.FromArgb(192, 187, 219);
            panelTitle.BackColor = Color.FromArgb(192, 187, 219);
            panelLogo.BackColor = Color.FromArgb(172, 167, 219);
            buttonLibrary.BackColor = Color.FromArgb(38, 38, 38);
            buttonAcc.BackColor = Color.FromArgb(38, 38, 38);
            buttonAdd.BackColor = Color.FromArgb(38, 38, 38);
            buttonEdit.BackColor = Color.FromArgb(38, 38, 38);
            OpenChildForm(new WindowsFormsApp1.FormFavor(), sender);
            pictureBoxClose.Visible = true;
            Change();
        }

        private void buttonAcc_Click(object sender, EventArgs e)
        {
            labelTitle.Text = "Авторизація";
            buttonAcc.BackColor = Color.FromArgb(238, 190, 212);
            panelTitle.BackColor = Color.FromArgb(238, 190, 212);
            panelLogo.BackColor = Color.FromArgb(218, 170, 212);
            buttonLibrary.BackColor = Color.FromArgb(38, 38, 38);
            buttonFavor.BackColor = Color.FromArgb(38, 38, 38);
            buttonAdd.BackColor = Color.FromArgb(38, 38, 38);
            buttonEdit.BackColor = Color.FromArgb(38, 38, 38);
            if(GlobalUser != null)
            {
                OpenChildForm(new Forms.FormAcc(), sender);
            }
            else
                OpenChildForm(new Forms.FormSignIN(), sender);
            pictureBoxClose.Visible = true;
            Change();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            labelTitle.Text = "Адміністратор";
            buttonAdd.BackColor = Color.FromArgb(169, 112, 144);
            panelTitle.BackColor = Color.FromArgb(169, 112, 144);
            panelLogo.BackColor = Color.FromArgb(149, 92, 144);
            buttonLibrary.BackColor = Color.FromArgb(38, 38, 38);
            buttonAcc.BackColor = Color.FromArgb(38, 38, 38);
            buttonFavor.BackColor = Color.FromArgb(38, 38, 38);
            buttonEdit.BackColor = Color.FromArgb(38, 38, 38);
            OpenChildForm(new Forms.FormAdd(), sender);
            pictureBoxClose.Visible = true;
            Change();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            labelTitle.Text = "Адміністратор";
            buttonEdit.BackColor = Color.FromArgb(49, 85, 163);
            panelTitle.BackColor = Color.FromArgb(49, 85, 163);
            panelLogo.BackColor = Color.FromArgb(29, 65, 163);
            buttonLibrary.BackColor = Color.FromArgb(38, 38, 38);
            buttonAcc.BackColor = Color.FromArgb(38, 38, 38);
            buttonFavor.BackColor = Color.FromArgb(38, 38, 38);
            buttonAdd.BackColor = Color.FromArgb(38, 38, 38);
            OpenChildForm(new Forms.FormDE(), sender);
            pictureBoxClose.Visible = true;
            Change();
        }

        private void pictureBoxClose_Click(object sender, EventArgs e)
        {
            CloseChildForm();
        }

    }
}
