﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Library.Forms
{
    public partial class FormAdd : Form
    {
        public FormAdd()
        {
            InitializeComponent();
        }


        private string name, author, booklink;

        private void pictureBoxLoad_Click(object sender, EventArgs e)
        {

        }

        private void pictureBoxDone_Click(object sender, EventArgs e)
        {

        }

        private int year;
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            pictureBoxLoad.Visible = true;

            Book book = new Book();

            name = textBoxName.Text;
            author = textBoxAuthor.Text;
            if (!int.TryParse(textBoxYear.Text, out year))
            {
                labelError.Visible = true;
                
            }
            booklink = textBoxLink.Text;

            book.Add(name, author, year, booklink);

            pictureBoxLoad.Visible = false;
            pictureBoxDone.Visible = true;
            
        }
    }
}
