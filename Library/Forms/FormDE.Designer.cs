﻿
namespace Library.Forms
{
    partial class FormDE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxLink = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxYear = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxAuthor = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBoxDone1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxLoad1 = new System.Windows.Forms.PictureBox();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.pictureBoxDone2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxLoad2 = new System.Windows.Forms.PictureBox();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelMain = new System.Windows.Forms.Label();
            this.textBoxMain = new System.Windows.Forms.TextBox();
            this.labelMain2 = new System.Windows.Forms.Label();
            this.buttonMain = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxId = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.labelError = new System.Windows.Forms.Label();
            this.labelMainError = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDone1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLoad1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDone2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLoad2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxLink
            // 
            this.textBoxLink.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxLink.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxLink.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxLink.Location = new System.Drawing.Point(194, 265);
            this.textBoxLink.Name = "textBoxLink";
            this.textBoxLink.Size = new System.Drawing.Size(217, 32);
            this.textBoxLink.TabIndex = 106;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(42, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 29);
            this.label2.TabIndex = 95;
            this.label2.Text = "Назва:";
            // 
            // textBoxYear
            // 
            this.textBoxYear.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxYear.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxYear.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxYear.Location = new System.Drawing.Point(232, 204);
            this.textBoxYear.Name = "textBoxYear";
            this.textBoxYear.Size = new System.Drawing.Size(179, 32);
            this.textBoxYear.TabIndex = 105;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(42, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 29);
            this.label3.TabIndex = 96;
            this.label3.Text = "Автор:";
            // 
            // textBoxAuthor
            // 
            this.textBoxAuthor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxAuthor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxAuthor.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxAuthor.Location = new System.Drawing.Point(142, 144);
            this.textBoxAuthor.Name = "textBoxAuthor";
            this.textBoxAuthor.Size = new System.Drawing.Size(269, 32);
            this.textBoxAuthor.TabIndex = 104;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(42, 210);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(179, 29);
            this.label4.TabIndex = 97;
            this.label4.Text = "Рік написання:";
            // 
            // textBoxName
            // 
            this.textBoxName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxName.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxName.Location = new System.Drawing.Point(142, 90);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(269, 32);
            this.textBoxName.TabIndex = 103;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(42, 271);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(142, 29);
            this.label5.TabIndex = 98;
            this.label5.Text = "Посилання:";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label9.Location = new System.Drawing.Point(190, 271);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(221, 29);
            this.label9.TabIndex = 102;
            this.label9.Text = "________________";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(138, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(273, 29);
            this.label6.TabIndex = 99;
            this.label6.Text = "____________________";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label8.Location = new System.Drawing.Point(229, 210);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(182, 29);
            this.label8.TabIndex = 101;
            this.label8.Text = "_____________";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label7.Location = new System.Drawing.Point(138, 150);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(273, 29);
            this.label7.TabIndex = 100;
            this.label7.Text = "____________________";
            // 
            // pictureBoxDone1
            // 
            this.pictureBoxDone1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBoxDone1.Image = global::Library.Properties.Resources.premium_icon_tick_3106690;
            this.pictureBoxDone1.Location = new System.Drawing.Point(227, 355);
            this.pictureBoxDone1.Name = "pictureBoxDone1";
            this.pictureBoxDone1.Size = new System.Drawing.Size(103, 48);
            this.pictureBoxDone1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxDone1.TabIndex = 109;
            this.pictureBoxDone1.TabStop = false;
            this.pictureBoxDone1.Visible = false;
            // 
            // pictureBoxLoad1
            // 
            this.pictureBoxLoad1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBoxLoad1.Image = global::Library.Properties.Resources.premium_icon_cloud_download_2319152;
            this.pictureBoxLoad1.Location = new System.Drawing.Point(227, 355);
            this.pictureBoxLoad1.Name = "pictureBoxLoad1";
            this.pictureBoxLoad1.Size = new System.Drawing.Size(103, 48);
            this.pictureBoxLoad1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxLoad1.TabIndex = 108;
            this.pictureBoxLoad1.TabStop = false;
            this.pictureBoxLoad1.Visible = false;
            // 
            // buttonEdit
            // 
            this.buttonEdit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(85)))), ((int)(((byte)(163)))));
            this.buttonEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEdit.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonEdit.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.buttonEdit.Location = new System.Drawing.Point(75, 355);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(146, 48);
            this.buttonEdit.TabIndex = 107;
            this.buttonEdit.Text = "Редагувати";
            this.buttonEdit.UseVisualStyleBackColor = false;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // pictureBoxDone2
            // 
            this.pictureBoxDone2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBoxDone2.Image = global::Library.Properties.Resources.premium_icon_tick_3106690;
            this.pictureBoxDone2.Location = new System.Drawing.Point(606, 355);
            this.pictureBoxDone2.Name = "pictureBoxDone2";
            this.pictureBoxDone2.Size = new System.Drawing.Size(103, 48);
            this.pictureBoxDone2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxDone2.TabIndex = 112;
            this.pictureBoxDone2.TabStop = false;
            this.pictureBoxDone2.Visible = false;
            // 
            // pictureBoxLoad2
            // 
            this.pictureBoxLoad2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBoxLoad2.Image = global::Library.Properties.Resources.premium_icon_cloud_download_2319152;
            this.pictureBoxLoad2.Location = new System.Drawing.Point(606, 355);
            this.pictureBoxLoad2.Name = "pictureBoxLoad2";
            this.pictureBoxLoad2.Size = new System.Drawing.Size(103, 48);
            this.pictureBoxLoad2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxLoad2.TabIndex = 111;
            this.pictureBoxLoad2.TabStop = false;
            this.pictureBoxLoad2.Visible = false;
            // 
            // buttonDelete
            // 
            this.buttonDelete.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(85)))), ((int)(((byte)(163)))));
            this.buttonDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDelete.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonDelete.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.buttonDelete.Location = new System.Drawing.Point(454, 355);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(146, 48);
            this.buttonDelete.TabIndex = 113;
            this.buttonDelete.Text = "Видалити";
            this.buttonDelete.UseVisualStyleBackColor = false;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.Image = global::Library.Properties.Resources.premium_icon_research_1321070;
            this.pictureBox1.Location = new System.Drawing.Point(454, 96);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(255, 204);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 114;
            this.pictureBox1.TabStop = false;
            // 
            // labelMain
            // 
            this.labelMain.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMain.AutoSize = true;
            this.labelMain.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelMain.Location = new System.Drawing.Point(148, 188);
            this.labelMain.Name = "labelMain";
            this.labelMain.Size = new System.Drawing.Size(173, 29);
            this.labelMain.TabIndex = 115;
            this.labelMain.Text = "Введіть назву:";
            // 
            // textBoxMain
            // 
            this.textBoxMain.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxMain.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxMain.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxMain.Location = new System.Drawing.Point(331, 182);
            this.textBoxMain.Name = "textBoxMain";
            this.textBoxMain.Size = new System.Drawing.Size(269, 32);
            this.textBoxMain.TabIndex = 117;
            // 
            // labelMain2
            // 
            this.labelMain2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMain2.AutoSize = true;
            this.labelMain2.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelMain2.Location = new System.Drawing.Point(327, 188);
            this.labelMain2.Name = "labelMain2";
            this.labelMain2.Size = new System.Drawing.Size(273, 29);
            this.labelMain2.TabIndex = 116;
            this.labelMain2.Text = "____________________";
            // 
            // buttonMain
            // 
            this.buttonMain.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(85)))), ((int)(((byte)(163)))));
            this.buttonMain.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonMain.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMain.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonMain.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.buttonMain.Location = new System.Drawing.Point(327, 252);
            this.buttonMain.Name = "buttonMain";
            this.buttonMain.Size = new System.Drawing.Size(146, 48);
            this.buttonMain.TabIndex = 118;
            this.buttonMain.Text = "Пошук";
            this.buttonMain.UseVisualStyleBackColor = false;
            this.buttonMain.Click += new System.EventHandler(this.buttonMain_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(42, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 29);
            this.label1.TabIndex = 119;
            this.label1.Text = "Id:";
            // 
            // textBoxId
            // 
            this.textBoxId.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxId.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBoxId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxId.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxId.Location = new System.Drawing.Point(91, 42);
            this.textBoxId.Name = "textBoxId";
            this.textBoxId.ReadOnly = true;
            this.textBoxId.Size = new System.Drawing.Size(38, 32);
            this.textBoxId.TabIndex = 121;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label10.Location = new System.Drawing.Point(87, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 29);
            this.label10.TabIndex = 120;
            this.label10.Text = "__";
            // 
            // labelError
            // 
            this.labelError.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelError.AutoSize = true;
            this.labelError.ForeColor = System.Drawing.Color.Firebrick;
            this.labelError.Location = new System.Drawing.Point(113, 309);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(199, 20);
            this.labelError.TabIndex = 122;
            this.labelError.Text = "Неправильно введені дані!";
            this.labelError.Visible = false;
            // 
            // labelMainError
            // 
            this.labelMainError.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMainError.AutoSize = true;
            this.labelMainError.ForeColor = System.Drawing.Color.Firebrick;
            this.labelMainError.Location = new System.Drawing.Point(299, 309);
            this.labelMainError.Name = "labelMainError";
            this.labelMainError.Size = new System.Drawing.Size(199, 20);
            this.labelMainError.TabIndex = 123;
            this.labelMainError.Text = "Неправильно введені дані!";
            this.labelMainError.Visible = false;
            // 
            // FormDE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HighlightText;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelMainError);
            this.Controls.Add(this.labelError);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxId);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.buttonMain);
            this.Controls.Add(this.labelMain);
            this.Controls.Add(this.textBoxMain);
            this.Controls.Add(this.labelMain2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.pictureBoxDone2);
            this.Controls.Add(this.pictureBoxLoad2);
            this.Controls.Add(this.pictureBoxDone1);
            this.Controls.Add(this.pictureBoxLoad1);
            this.Controls.Add(this.buttonEdit);
            this.Controls.Add(this.textBoxLink);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxYear);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxAuthor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Name = "FormDE";
            this.Text = "FormDE";
            this.Load += new System.EventHandler(this.FormDE_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDone1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLoad1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDone2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLoad2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxLink;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxYear;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxAuthor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBoxDone1;
        private System.Windows.Forms.PictureBox pictureBoxLoad1;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.PictureBox pictureBoxDone2;
        private System.Windows.Forms.PictureBox pictureBoxLoad2;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelMain;
        private System.Windows.Forms.TextBox textBoxMain;
        private System.Windows.Forms.Label labelMain2;
        private System.Windows.Forms.Button buttonMain;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxId;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelError;
        private System.Windows.Forms.Label labelMainError;
    }
}