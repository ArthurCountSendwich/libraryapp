﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using Microsoft.EntityFrameworkCore;

namespace Library.Forms
{
    public partial class FormLibrary : Form
    {
        public FormLibrary()
        {
            InitializeComponent();
        }

        private void Search()
        {
            dataGridViewLibrary.Rows.Clear();
            Book book = new Book();
            string text = textBoxSearch.Text;
            var res = book.GetAllFromName(text);
            int n = 1;
            foreach (Book book1 in res)
            {
                dataGridViewLibrary.Rows.Add($"{n}", book1.Name, book1.Author, book1.Year, "Читати");
                n++;
            }
            res = book.GetAllFromAuthor(text);
            foreach (Book book1 in res)
            {
                dataGridViewLibrary.Rows.Add($"{n}", book1.Name, book1.Author, book1.Year, "Читати");
                n++;
            }
            pictureBoxBack.Visible = false;
            dataGridViewLibrary.Visible = true;

            if(dataGridViewLibrary.Rows[0].Cells[0].Value == null)
            {
                pictureBoxBack.Visible = true;
                dataGridViewLibrary.Visible = false;
                labelError.Visible = true;
            }
        }
        private void pictureBoxSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void dataGridViewLibrary_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == 4)
            {
                string name = (dataGridViewLibrary.Rows[e.RowIndex].Cells[1].Value).ToString();
                Book book = new Book();
                var res = book.GetFromName(name);
                Form1.GlobalUser.BookId = res.BookId;
                User user = new User();
                user.Edit(Form1.GlobalUser.login, Form1.GlobalUser.password, Form1.GlobalUser.Name, Form1.GlobalUser.Surname, Form1.GlobalUser.DateofBirth, Form1.GlobalUser.male, Form1.GlobalUser.IsAdmin, Form1.GlobalUser.BookId);

                pictureBoxDone.Visible = true;
            }
        }
    }
}
