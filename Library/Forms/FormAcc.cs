﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Library.Forms
{
    public partial class FormAcc : Form
    {
        public FormAcc()
        {
            InitializeComponent();
        }

        private void FormAcc_Load(object sender, EventArgs e)
        {
            textBoxName.Text = Form1.GlobalUser.Name;
            textBoxSurname.Text = Form1.GlobalUser.Surname;
            textBoxDate.Text = Form1.GlobalUser.DateofBirth;
            textBoxLogin.Text = Form1.GlobalUser.login;
            textBoxPassword.Text = Form1.GlobalUser.password;
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            string NewName, NewSurname, NewDate, Login, NewPassword;
            bool NewIsMale, NewIsAdmin;
            int NewBookId;
            NewName = textBoxName.Text;
            NewSurname = textBoxSurname.Text;
            NewDate = textBoxDate.Text;
            Login = textBoxLogin.Text;
            NewPassword = textBoxPassword.Text;
            NewIsMale = Form1.GlobalUser.male;
            NewIsAdmin = Form1.GlobalUser.IsAdmin;
            NewBookId = Form1.GlobalUser.BookId;

            User user = new User();
            user.Edit(Login, NewPassword, NewName, NewSurname, NewDate, NewIsMale, NewIsAdmin, NewBookId);
            pictureBoxDone1.Visible = true;
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            string Login = textBoxLogin.Text;
            User user = new User();
            user.Delete(Login);
            Form1.GlobalUser = null;
            pictureBoxDone2.Visible = true;
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Form1.GlobalUser = null;
            pictureBoxDone3.Visible = true;
        }
    }
}
