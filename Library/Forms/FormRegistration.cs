﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Library.Forms
{
    public partial class FormRegistration : Form
    {
        public FormRegistration()
        {
            InitializeComponent();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            pictureBoxLoad.Visible = true;

            string name, surname, date, login, password;
            bool male;
            name = textBoxName.Text;
            surname = textBoxSurname.Text;
            date = $"{textBoxDay.Text}.{textBoxMounth.Text}.{textBoxYear.Text}";
            login = textBoxLogin.Text;
            password = textBoxPassword.Text;
            if (radioButtonMale.Checked)
                male = true;
            else if (radioButtonFemale.Checked)
                male = false;
            else
                male = true;

            User user = new User();
            user.Add(name, surname, date, male, login, password);

            pictureBoxLoad.Visible = false;
            pictureBoxDone.Visible = true;
        }

    }
}
