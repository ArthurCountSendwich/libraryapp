﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Library.Forms
{
    public partial class FormDE : Form
    {
        private string name;
        public FormDE()
        {
            InitializeComponent();
        }

        private void FormDE_Load(object sender, EventArgs e)
        {
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            label5.Visible = false;
            label6.Visible = false;
            label7.Visible = false;
            label8.Visible = false;
            label9.Visible = false;
            label10.Visible = false;
            textBoxId.Visible = false;
            textBoxName.Visible = false;
            textBoxAuthor.Visible = false;
            textBoxYear.Visible = false;
            textBoxLink.Visible = false;
            buttonEdit.Visible = false;
            buttonDelete.Visible = false;
            pictureBox1.Visible = false;
        }

        private void buttonMain_Click(object sender, EventArgs e)
        {
            labelMainError.Visible = false;
            name = textBoxMain.Text;
            Book book = new Book();
            var book1 = book.GetFromName(name);

            if (book1 == null)
            {
                labelMainError.Visible = true;
            }
            else if (book1 != null)
            {
                textBoxId.Text = $"{book1.BookId}";
                textBoxName.Text = book1.Name;
                textBoxAuthor.Text = book1.Author;
                textBoxYear.Text = $"{book1.Year}";
                textBoxLink.Text = book1.BookLink;


                label1.Visible = true;
                label2.Visible = true;
                label3.Visible = true;
                label4.Visible = true;
                label5.Visible = true;
                label6.Visible = true;
                label7.Visible = true;
                label8.Visible = true;
                label9.Visible = true;
                label10.Visible = true;
                textBoxId.Visible = true;
                textBoxName.Visible = true;
                textBoxAuthor.Visible = true;
                textBoxYear.Visible = true;
                textBoxLink.Visible = true;
                buttonEdit.Visible = true;
                buttonDelete.Visible = true;
                pictureBox1.Visible = true;
                labelMain.Visible = false;
                labelMain2.Visible = false;
                textBoxMain.Visible = false;
                buttonMain.Visible = false;
            }
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            pictureBoxLoad1.Visible = true;

            string NewName, NewAuthor, NewBookLink;
            int NewYear;
            NewName = textBoxName.Text;
            NewAuthor = textBoxAuthor.Text;
            if (!int.TryParse(textBoxYear.Text, out NewYear))
            {
                labelError.Visible = true;
            }
            NewBookLink = textBoxLink.Text;

            Book book = new Book();
            book.Edit(name, NewName, NewAuthor, NewYear, NewBookLink);

            pictureBoxLoad1.Visible = false;
            pictureBoxDone1.Visible = true;
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            pictureBoxLoad2.Visible = true;

            Book book = new Book();
            book.Delete(name);

            pictureBoxLoad2.Visible = false;
            pictureBoxDone2.Visible = true;
        }
    }
}
