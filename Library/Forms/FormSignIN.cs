﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Library.Forms
{
    public partial class FormSignIN : Form
    {
        private Form activeForm;
        public FormSignIN()
        {
            InitializeComponent();
        }
        private void OpenChildForm(Form childForm, object buttonSender)
        {
            if (activeForm != null)
            {
                activeForm.Close();
            }
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            this.panelDesktop.Controls.Add(childForm);
            this.panelDesktop.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
            panelDesktop.Visible = true;
            panelDesktop.Dock = DockStyle.Fill;
        }
        private void CloseChildForm()
        {
            if (activeForm != null)
            {
                activeForm.Close();
            }
            panelDesktop.Visible = false;
        }
        private void FormAccount_Load(object sender, EventArgs e)
        {
            textBoxPassword.UseSystemPasswordChar = true;
            pictureBoxHide.Visible = false;
        }

        private void pictureBoxOpen_Click(object sender, EventArgs e)
        {
            textBoxPassword.UseSystemPasswordChar = false;
            pictureBoxHide.Visible = true;
            pictureBoxOpen.Visible = false;
        }

        private void pictureBoxHide_Click(object sender, EventArgs e)
        {
            textBoxPassword.UseSystemPasswordChar = true;
            pictureBoxHide.Visible = false;
            pictureBoxOpen.Visible = true;
        }

        private void labelRegister_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.FormRegistration(), sender);
        }

        private void buttonRegister_Click(object sender, EventArgs e)
        {
            string login, password;
            login = textBoxLogin.Text;
            password = textBoxPassword.Text;

            User user = new User();
            var user1 = user.SignIn(login, password);
            if (user1 != null)
            {
                Form1.GlobalUser = user1;
                pictureBoxDone.Visible = true;
                
            }
            else
                labelError.Visible = true;
        }
    }
}
