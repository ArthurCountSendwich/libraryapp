﻿using System;
using Microsoft.EntityFrameworkCore;

namespace Library
{
    class LibraryContext : DbContext
    {
        public DbSet<Book> Books { set; get; }
        public DbSet<User> Users { set; get; }
        public LibraryContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server = DESKTOP-E2KCMDL; Database = Library; Trusted_Connection = True");
        }

    }
}
