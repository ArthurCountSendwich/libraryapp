﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Library
{
    public class Book
    {
        public int BookId { set; get; }
        public string Name { set; get; }
        public string Author { set; get; }
        public int Year { set; get; }
        public string BookLink { set; get; }
        public virtual ICollection<User> Users { set; get; }

        public Book()
        {
            Name = Name;
            Author = Author;
            Year = Year;
            BookLink = BookLink;
        }
        public Book Add(string name, string author, int year, string bookLink)
        {
            using (LibraryContext db = new LibraryContext())
            {
                Book book = new Book {Name = name, Author = author, Year = year, BookLink = bookLink };

                db.Books.Add(book);
                db.SaveChanges();
                return book;
            }
        }
        
        public Book GetFromName(string name)
        {
            using(LibraryContext db = new LibraryContext())
            {
                var book = db.Books.FirstOrDefault(x => x.Name == name);

                return book;
            }
        }
        public Book GetFromId(int bookId)
        {
            using (LibraryContext db = new LibraryContext())
            {
                var book = db.Books.FirstOrDefault(x => x.BookId == bookId);

                return book;
            }
        }
        public List<Book> GetAllFromName(string text)
        {
            using (LibraryContext db = new LibraryContext())
            {
                var books = db.Books.Where(x => EF.Functions.Like(x.Name, $"%{text}%")).ToList();
                return books;
            }
        }
        public List<Book> GetAllFromAuthor(string text)
        {
            using (LibraryContext db = new LibraryContext())
            {
                var books = db.Books.Where(x => EF.Functions.Like(x.Author, $"%{text}%")).ToList();
                return books;
            }
        }

        public Book Delete(string name)
        {
            using(LibraryContext db = new LibraryContext())
            {
                var book = db.Books.FirstOrDefault(x => x.Name == name);

                if(book != null)
                {
                    db.Books.Remove(book);
                    db.SaveChanges();
                }

                return null;
            }
        }

        public Book Edit(string name, string NewName, string NewAuthor, int NewYear, string NewBookLink)
        {
            using (LibraryContext db = new LibraryContext())
            {
                var book = db.Books.FirstOrDefault(x => x.Name == name);

                book.Name = NewName;
                book.Author = NewAuthor;
                book.Year = NewYear;
                book.BookLink = NewBookLink;

                db.Books.Update(book);
                db.SaveChanges();

                return book;
            }
        }
    }
}
