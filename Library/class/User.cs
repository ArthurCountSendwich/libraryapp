﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Library

{
    public class User
    {
        public int UserId { set; get; }
        public string Name { set; get; }
        public string Surname { set; get; }
        public string DateofBirth { set; get; }
        public bool male { set; get; }
        public bool IsAdmin { set; get; }
        public string login { set; get; }
        public string password { set; get; }
        public int BookId { set; get; }
        public virtual Book Book { set; get; }

        public User()
        {
            Name = Name;
            Surname = Surname;
            DateofBirth = DateofBirth;
            male = male;
            IsAdmin = IsAdmin;
            login = login;
            password = password;
            BookId = BookId;
        }
        public User Add(string name, string surname, string dateofBirth, bool Male, string Login, string Password)
        {
            using (LibraryContext db = new LibraryContext())
            {
                User user = new User { Name = name, Surname = surname, DateofBirth = dateofBirth, male = Male, IsAdmin = false, login = Login, password = Password, BookId = 1 };

                db.Users.Add(user);
                db.SaveChanges();
                return user;
            }
        }
        public User Get(string Login)
        {
            using (LibraryContext db = new LibraryContext())
            {
                var user = db.Users.FirstOrDefault(x => x.login == Login);

                return user;
            }
        }
        public User SignIn(string Login, string Password)
        {
            using (LibraryContext db = new LibraryContext())
            {
                var user = db.Users.FirstOrDefault(x => x.login == Login);

                if (user != null)
                {
                    if (user.password == Password)
                    {
                        return user;
                    }
                    else
                        return null;
                }
                else
                    return null;
            }
        }
        public User Delete(string Login)
        {
            using (LibraryContext db = new LibraryContext())
            {
                var user = db.Users.FirstOrDefault(x => x.login == Login);

                if (user != null)
                {
                    db.Users.Remove(user);
                    db.SaveChanges();
                }

                return null;
            }
        }

        public User Edit(string Login, string NewPassword, string NewName, string NewSurname, string NewDate, bool NewMale, bool NewIsAdmin, int NewBookId)
        {
            using (LibraryContext db = new LibraryContext())
            {
                var user = db.Users.FirstOrDefault(x => x.login == Login);

                user.Name = NewName;
                user.Surname = NewSurname;
                user.DateofBirth = NewDate;
                user.male = NewMale;
                user.IsAdmin = NewIsAdmin;
                user.login = Login;
                user.password = NewPassword;
                user.BookId = NewBookId;

                db.Users.Update(user);
                db.SaveChanges();

                return user;
            }
        }
    }
    
    
}
