﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FormFavor : Form
    {
        static public string BookLInk;
        public FormFavor()
        {
            InitializeComponent();
        }

        private void FormFavor_Load(object sender, EventArgs e)
        {
            if(BookLInk != null)
            {
                if(BookLInk == "none")
                {
                    webBrowser1.Visible = false;
                    pictureBox1.Visible = true;
                    labelNoAcc.Visible = false;
                    labelNoBook.Visible = true;
                }
                else
                {
                    pictureBox1.Visible = false;
                    labelNoAcc.Visible = false;
                    labelNoBook.Visible = false;
                    webBrowser1.Visible = true;
                    webBrowser1.Navigate($"{BookLInk}");
                }
            }
            else
            {
                webBrowser1.Visible = false;
                pictureBox1.Visible = true;
                labelNoAcc.Visible = true;
                labelNoBook.Visible = false;
            }
        }
    }
}
