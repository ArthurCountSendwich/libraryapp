﻿
namespace WindowsFormsApp1
{
    partial class FormFavor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNoAcc = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelNoBook = new System.Windows.Forms.Label();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelNoAcc
            // 
            this.labelNoAcc.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNoAcc.AutoSize = true;
            this.labelNoAcc.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.labelNoAcc.Location = new System.Drawing.Point(270, 82);
            this.labelNoAcc.Name = "labelNoAcc";
            this.labelNoAcc.Size = new System.Drawing.Size(263, 36);
            this.labelNoAcc.TabIndex = 1;
            this.labelNoAcc.Text = "Війдіть в аккаунт";
            this.labelNoAcc.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::WindowsFormsApp1.Properties.Resources.reading;
            this.pictureBox1.Location = new System.Drawing.Point(1, 121);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(796, 267);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // labelNoBook
            // 
            this.labelNoBook.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNoBook.AutoSize = true;
            this.labelNoBook.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.labelNoBook.Location = new System.Drawing.Point(283, 82);
            this.labelNoBook.Name = "labelNoBook";
            this.labelNoBook.Size = new System.Drawing.Size(233, 36);
            this.labelNoBook.TabIndex = 3;
            this.labelNoBook.Text = "Виберіть книгу";
            this.labelNoBook.Visible = false;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(800, 450);
            this.webBrowser1.TabIndex = 4;
            this.webBrowser1.Visible = false;
            // 
            // FormFavor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HighlightText;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.labelNoBook);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.labelNoAcc);
            this.Name = "FormFavor";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FormFavor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNoAcc;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelNoBook;
        private System.Windows.Forms.WebBrowser webBrowser1;
    }
}

